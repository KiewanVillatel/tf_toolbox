import tensorflow as tf
from tensorflow.python.ops import variable_scope as vs
from tensorflow.python.ops import init_ops
from tensorflow.contrib.layers.python.layers import layers


def rnn_mlp_module(input, input_size, output_size, hidden_layer_sizes, scope, layer_norm=True, reuse=False):
  with vs.variable_scope(scope, reuse=reuse):
    for i in range(0, len(hidden_layer_sizes) + 1):
      with vs.variable_scope('linear_' + str(i + 1)):
        b_size = hidden_layer_sizes[i] if i != len(hidden_layer_sizes) else output_size
        vs.get_variable('b', [1, b_size], dtype=tf.float32)

        w_in_size = input_size if i == 0 else hidden_layer_sizes[i - 1]
        w_out_size = output_size if i == len(hidden_layer_sizes) else hidden_layer_sizes[i]
        vs.get_variable('w', [w_in_size, w_out_size], dtype=tf.float32)

        if layer_norm:
          gamma_init = init_ops.constant_initializer(1.0)
          beta_init = init_ops.constant_initializer(0.0)
          with vs.variable_scope('layer_norm'):
            shape = [w_out_size]
            # Initialize beta and gamma for use by layer_norm.
            vs.get_variable("gamma", shape=shape, initializer=gamma_init)
            vs.get_variable("beta", shape=shape, initializer=beta_init)

  def map_output(elem):
    with vs.variable_scope(scope, reuse=True):
      # feed forward network
      _layers = []
      for i in range(0, len(hidden_layer_sizes) + 1):
        with vs.variable_scope('linear_' + str(i + 1)):
          inputs = elem if i == 0 else _layers[i - 1]

          b = vs.get_variable('b', dtype=tf.float32)

          w = vs.get_variable('w', dtype=tf.float32)

          _l = tf.matmul(inputs, w) + b

          if layer_norm:
            _l = layers.layer_norm(_l, scope='layer_norm')

          if i != len(hidden_layer_sizes):
            _l = tf.nn.tanh(_l)
          _layers.append(_l)

    return _layers[-1]

  transposed_inputs = tf.transpose(input, (1, 0, 2))
  preds = tf.map_fn(map_output, transposed_inputs)
  preds = tf.transpose(preds, (1, 0, 2))

  return preds
