from tensorflow.contrib.layers.python.layers import layers
from tensorflow.python.ops import array_ops, math_ops, nn_ops, init_ops
from tensorflow.python.ops import rnn_cell_impl


class MinimalRNNCell(rnn_cell_impl.RNNCell):
  """Minimal RNN unit with layer normalization.
    Minimal RNN cell implementation is based on:
    https://arxiv.org/abs/1711.06788
    "MinimalRNN: Toward More Interpretable and Trainable Recurrent Neural Networks"
    Chen, M. (2017).
    Layer normalization implementation is based on:
      https://arxiv.org/abs/1607.06450.
    "Layer Normalization"
    Jimmy Lei Ba, Jamie Ryan Kiros, Geoffrey E. Hinton
    and is applied before the internal nonlinearities.
    """

  def __init__(self, num_units,
               activation=math_ops.tanh,
               layer_norm=True,
               norm_gain=1.0,
               norm_shift=0.0,
               reuse=None,
               dropout_keep_prob=1):
    """Initializes the cell.
        Args:
          num_units: int, The number of units in the cell.
          activation: Activation function of the inner states.
          layer_norm: If `True`, layer normalization will be applied.
          norm_gain: float, The layer normalization gain initial value. If
            `layer_norm` has been set to `False`, this argument will be ignored.
          norm_shift: float, The layer normalization shift initial value. If
            `layer_norm` has been set to `False`, this argument will be ignored.
        """

    super(MinimalRNNCell, self).__init__(_reuse=reuse)

    self._num_units = num_units
    self._activation = activation
    self._layer_norm = layer_norm
    self._g = norm_gain
    self._b = norm_shift
    self._reuse = reuse

  @property
  def state_size(self):
    return self._num_units

  @property
  def output_size(self):
    return self._num_units

  def _linear(self, args, kernel, bias):
    out = math_ops.matmul(args, kernel)
    if not self._layer_norm:
      out = nn_ops.bias_add(out, bias)
    return out

  def _norm(self, inp, scope):
    return layers.layer_norm(inp, reuse=True, scope=scope)

  def build(self, inputs_shape):
    if inputs_shape[1].value is None:
      raise ValueError("Expected inputs.shape[-1] to be known, saw shape: %s"
                       % inputs_shape)

    input_depth = inputs_shape[1].value

    # Initialize beta and gamma for use by layer_norm.
    if self._layer_norm:
      scopes = ["update_gate", "candidate"]
      for scope in scopes:
        self.add_variable(scope + "/gamma",
                          shape=[self._num_units],
                          initializer=init_ops.constant_initializer(self._g))
        self.add_variable(scope + "/beta",
                          shape=[self._num_units],
                          initializer=init_ops.constant_initializer(self._b))

    self.update_gate_kernel = self.add_variable(
      "update_gate/kernel",
      shape=[input_depth + self._num_units, self._num_units])

    self.candidate_kernel = self.add_variable(
      "candidate/kernel",
      shape=[input_depth, self._num_units])

    self.update_gate_bias = self.add_variable(
      "update_gate/bias",
      shape=[self._num_units]) if not self._layer_norm else None

    self.candidate_bias = self.add_variable(
      "candidate/bias",
      shape=[self._num_units]) if not self._layer_norm else None

    self.built = True

  def call(self, inputs, state):
    """GRU cell with layer normalization."""

    args = array_ops.concat([inputs, state], 1)

    u = self._linear(args,
                     kernel=self.update_gate_kernel,
                     bias=self.update_gate_bias)

    candidate = self._linear(inputs, kernel=self.candidate_kernel, bias=self.candidate_bias)

    if self._layer_norm:
      u = self._norm(u, "update_gate")
      candidate = self._norm(candidate, "candidate")

    u = math_ops.sigmoid(u)
    candidate = self._activation(candidate)

    new_h = u * state + (1 - u) * candidate

    return new_h, new_h
