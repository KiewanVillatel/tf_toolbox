import tensorflow as tf


class MetricOp(object):
  def __init__(self, name, value, update, reset):
    self._name = name
    self._value = value
    self._update = update
    self._reset = reset

  @property
  def name(self):
    return self._name

  @property
  def value(self):
    return self._value

  @property
  def update(self):
    return self._update

  @property
  def reset(self):
    return self._reset


def create_metric(scope, metric, **metric_args):
  with tf.variable_scope(scope) as scope:
    metric_op, update_op = metric(**metric_args)
    scope_vars = tf.get_collection(scope=scope.original_name_scope, key=tf.GraphKeys.LOCAL_VARIABLES)

    if len(scope_vars) == 0:
      raise Exception("No local variables found.")

    reset_op = tf.variables_initializer(scope_vars)
  return MetricOp('MetricOp', metric_op, update_op, reset_op)
