import tensorflow as tf
from tensorflow.python.ops import math_ops

from .ops import log2


def prediction_stability(labels, predictions, weights):  # [B, T, O]
  mask = tf.tile(input=weights, multiples=[1, predictions.shape[-1]])
  mask = tf.reshape(tensor=mask, shape=predictions.shape)

  masked_preds = tf.multiply(predictions, mask)

  sum_similarities = tf.get_variable(name="sum_similarities",
                                     shape=(),
                                     dtype=tf.float64,
                                     initializer=tf.zeros_initializer,
                                     collections=[tf.GraphKeys.LOCAL_VARIABLES])

  nb_preds = tf.get_variable(name="nb_preds",
                             shape=(),
                             dtype=tf.float64,
                             initializer=tf.zeros_initializer,
                             collections=[tf.GraphKeys.LOCAL_VARIABLES])

  res = tf.div(sum_similarities, nb_preds)

  current_preds = masked_preds[:, :-1, :]
  next_preds = masked_preds[:, 1:, :]

  num = tf.reduce_sum(tf.multiply(current_preds, next_preds) * 100, axis=-1)
  norm1 = tf.sqrt(tf.reduce_sum(current_preds ** 2, axis=-1))
  norm2 = tf.sqrt(tf.reduce_sum(next_preds ** 2, axis=-1))
  denom = tf.multiply(norm1, norm2)

  similarities = tf.reduce_sum(tf.div(num, 10e-12 + denom))

  sum_similarities = sum_similarities.assign_add(tf.cast(similarities, tf.float64))
  nb_preds = nb_preds.assign_add(tf.cast(tf.reduce_sum(weights) - int(predictions.shape[0]), tf.float64))

  update = tf.div(sum_similarities, nb_preds)

  return res, update


def accuracy(labels, predictions, weights, k=1, n=1):  # [B, T, O]
  """
  Compute "accuracy @k". Prediction in current timestep is considered as correct if one of the top_k predicted items
  is present in the next n labels
  :param n:
  :param k:
  :param labels:
  :param predictions:
  :param weights:
  :return:
  """

  _, top_k_preds = tf.nn.top_k(predictions, k, sorted=False)

  top_k_preds += 1
  labels = labels + 1
  labels = tf.cast(tf.multiply(tf.cast(labels, tf.float32), weights), tf.int32)

  correct_preds = tf.get_variable(name="true_positives", shape=(), dtype=tf.float32, initializer=tf.zeros_initializer,
                                  collections=[tf.GraphKeys.LOCAL_VARIABLES])
  nb_preds = tf.get_variable(name="nb_preds", shape=(), dtype=tf.float32, initializer=tf.zeros_initializer,
                             collections=[tf.GraphKeys.LOCAL_VARIABLES])

  res = tf.cast(correct_preds, tf.float32) / nb_preds

  batch_correct_preds = 0

  for i in range(top_k_preds.shape[1]):
    top_k_pred = top_k_preds[:, i]
    target_labels = labels[:, i:i + n]
    intersect = tf.sets.set_intersection(top_k_pred, target_labels)
    intersect = tf.sparse_tensor_to_dense(intersect)
    non_zeros = tf.count_nonzero(intersect, axis=1, dtype=tf.int32)
    batch_correct_preds += tf.reduce_sum(tf.minimum(tf.ones_like(non_zeros, dtype=tf.int32), non_zeros))

  correct_preds = correct_preds.assign_add(tf.cast(batch_correct_preds, tf.float32))
  mask_sum = tf.reduce_sum(weights)

  nb_preds = nb_preds.assign_add(mask_sum)

  update = math_ops.div(correct_preds, nb_preds, name='update')

  return res, update


def recall(labels, predictions, weights, k=1, n=1):
  _, top_k_preds = tf.nn.top_k(predictions, k, sorted=False)
  top_k_preds += 1
  labels = labels + 1
  labels = tf.cast(tf.multiply(tf.cast(labels, tf.float32), weights), tf.int32)

  lengths = tf.reduce_sum(weights, axis=1)

  batch_relevant_items_counts = tf.zeros(shape=(), dtype=tf.int32)
  batch_correct_preds = tf.zeros(shape=(), dtype=tf.int32)

  for i in range(top_k_preds.shape[1]):
    top_k_pred = top_k_preds[:, i:i + 1]
    target_labels = labels[:, i:i + n]

    # Check if enough events are lefts in the sequences
    valid_sequences_mask = tf.greater(lengths - i * tf.ones_like(lengths), n * tf.ones_like(lengths))

    relevant_items_count = tf.sets.set_size(tf.sets.set_union(target_labels, tf.zeros_like(target_labels))) - 1
    relevant_items_count = tf.boolean_mask(tensor=relevant_items_count, mask=valid_sequences_mask)

    batch_relevant_items_counts += tf.reduce_sum(relevant_items_count)

    intersect = tf.sets.set_intersection(top_k_pred, target_labels)
    intersect = tf.boolean_mask(tensor=tf.sparse_tensor_to_dense(intersect), mask=valid_sequences_mask)
    batch_correct_preds += tf.count_nonzero(intersect, dtype=tf.int32)

  return tf.cast(batch_correct_preds, tf.float32) / (tf.cast(batch_relevant_items_counts, tf.float32) + 1e-12), \
         batch_correct_preds, \
         batch_relevant_items_counts


def recall_streaming(labels, predictions, weights, k=1, n=1):  # [B, T, O]
  """
  Compute "recall @k"
  :param n:
  :param k:
  :param labels:
  :param predictions:
  :param weights:
  :return:
  """

  _, top_k_preds = tf.nn.top_k(predictions, k, sorted=False)
  top_k_preds += 1
  labels = labels + 1
  labels = tf.cast(tf.multiply(tf.cast(labels, tf.float32), weights), tf.int32)

  correct_preds = tf.get_variable(name="true_positives", shape=(), dtype=tf.int32, initializer=tf.zeros_initializer,
                                  collections=[tf.GraphKeys.LOCAL_VARIABLES])
  relevant_items_counts = tf.get_variable(name="nb_preds", shape=(), dtype=tf.int32, initializer=tf.zeros_initializer,
                                          collections=[tf.GraphKeys.LOCAL_VARIABLES])

  res = tf.cast(correct_preds, tf.float32) / tf.cast(relevant_items_counts, tf.float32)

  batch_recalls = []
  batch_relevant_items_counts = tf.zeros(shape=(), dtype=tf.int32)

  lengths = tf.reduce_sum(weights, axis=1)

  for i in range(top_k_preds.shape[1]):
    top_k_pred = top_k_preds[:, i]
    target_labels = labels[:, i:i + n]

    # Check if enough events are lefts in the sequences
    valid_sequences_mask = tf.greater(lengths - i * tf.ones_like(lengths), n * tf.ones_like(lengths))

    relevant_items_count = tf.sets.set_size(tf.sets.set_union(target_labels, tf.zeros_like(target_labels))) - 1
    relevant_items_count = tf.boolean_mask(tensor=relevant_items_count, mask=valid_sequences_mask)

    batch_relevant_items_counts += tf.reduce_sum(relevant_items_count)
    intersect = tf.sets.set_intersection(top_k_pred, target_labels)
    intersect = tf.boolean_mask(tensor=tf.sparse_tensor_to_dense(intersect), mask=valid_sequences_mask)
    batch_recalls.append(tf.count_nonzero(intersect, dtype=tf.int32))

  batch_correct_preds = tf.reduce_sum(tf.stack(batch_recalls))

  relevant_items_counts = relevant_items_counts.assign_add(batch_relevant_items_counts)

  correct_preds = correct_preds.assign_add(batch_correct_preds)

  update = math_ops.div(tf.cast(correct_preds, tf.float32), tf.cast(relevant_items_counts, tf.float32), name='update')

  return res, update


# TODO: transform into streaming metric
def precision(labels, logits, mask, k=1, n=1):  # [B, T, O]
  """
  Compute "precision @k". Prediction in current timestep is considered as correct if one of the top_k predicted items
  is present in the next n labels
  :param n:
  :param k:
  :param labels:
  :param logits:
  :param mask:
  :return:
  """

  _, top_k_preds = tf.nn.top_k(logits, k, sorted=False)
  top_k_preds += 1
  labels = labels + 1
  labels = tf.cast(tf.multiply(tf.cast(labels, tf.float32), mask), tf.int32)

  correct_preds = []

  for i in range(top_k_preds.shape[1]):
    top_k_pred = top_k_preds[:, i]
    target_labels = labels[:, i:i + n]
    intersect = tf.sets.set_intersection(top_k_pred, target_labels)
    intersect = tf.sparse_tensor_to_dense(intersect)
    correct_preds.append(tf.count_nonzero(intersect))

  nb_timesteps = tf.reduce_sum(mask)

  return tf.cast(tf.reduce_sum(correct_preds), tf.float32) / (k * nb_timesteps)


# TODO: transform into streaming metric
def st_on_lt_metric(metric, labels, logits, mask, k, n):
  """
  Compute the rapport between the short term metric (n=1) with the long term metric (n=n)
  :param metric:
  :param labels:
  :param logits:
  :param mask:
  :param k:
  :param n:
  :return:
  """
  return metric(labels, logits, mask, k, 1) / (metric(labels, logits, mask, k, n) + 1e-12)


# TODO: transform into streaming metric
def bpc(labels, logits):
  log_predictions = log2(logits)
  one_hot_labels = tf.one_hot(labels, depth=logits.shape[-1])
  bpc = tf.multiply(one_hot_labels, log_predictions)
  bpc = tf.reduce_sum(bpc, axis=2)
  bpc = -tf.reduce_mean(bpc)
  return bpc
