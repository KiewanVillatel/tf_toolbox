import tensorflow as tf


def log2(x):
  numerator = tf.log(x + 10e-12)
  denominator = tf.log(tf.constant(2, dtype=numerator.dtype))
  return numerator / denominator
